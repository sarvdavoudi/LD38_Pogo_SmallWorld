﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicActivatableMoveToTransform : LogicActivatable
{
    public Transform target;
    Vector3 startPosition;
    Quaternion startRot;
    Vector3 targetPosition;
    Quaternion targetRot;


    public float changeTime;
    protected override void Init()
    {
        startPosition = transform.position;
        startRot = transform.rotation;
        targetPosition = target.position;
        targetRot = target.rotation;
    }

    protected override void DoActivate()
    {
        StartCoroutine(Move());
    }

    private IEnumerator Move()
    {
        float elapsedTime = 0;

        while (elapsedTime <= changeTime)
        {
            transform.position = Vector3.Lerp(startPosition, targetPosition, (elapsedTime / changeTime));
            transform.rotation = Quaternion.Lerp(startRot, targetRot, (elapsedTime / changeTime));
            elapsedTime += Time.deltaTime;
            elapsedTime = Mathf.Clamp(elapsedTime, 0f, changeTime);
            yield return new WaitForEndOfFrame();
        }
        // finally, activate receiving elements
        Activate();
    }
}
