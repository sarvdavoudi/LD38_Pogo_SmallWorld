﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bit : MonoBehaviour {
    
    private PuzzleBit bit;
    private bool bitEnabled;
    private Material m;

    public Color active;
    public Color on;
    public Color off;

    private void Awake()
    {

        m = GetComponent<Renderer>().material;
        m.SetColor("_Color", new Color(0,0,0,0));
    }

    public void setBit(PuzzleBit _bit, Color _on, Color _active)
    {
        on = _on;
        active = _active;
        off = new Color(on.r, on.g, on.b, 0f);
        m.SetColor("_Color", off);
        bit = _bit;
    }
    
    public void OnChange () {
        if (bit != null)
        {
            if (bit.isLit())
            {
                m.SetColor("_EmissionColor", Color.gray);
                m.SetColor("_Color", active);
            }
            else
            {
                m.SetColor("_EmissionColor", Color.black);
                m.SetColor("_Color", on);
            }
        }
	}

    public void OnSolved()
    {
        if (bit != null)
        {
            m.SetColor("_EmissionColor", active);
        }
    }

    public void enable()
    {
        bitEnabled = true;
    }

    public void disable()
    {
        bitEnabled = false;
    }

    public bool isEnabled()
    {
        return bitEnabled;
    }
}
