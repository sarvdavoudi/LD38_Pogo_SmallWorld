﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicActivatableColourChange : LogicActivatable
{
    public Color off;
    public Color on;
    public float changeTime;

    private Material m;
    private Color current;

    protected override void Init()
    {
        m = GetComponent<Renderer>().material;
        m.color = off;
    }

    protected override void DoActivate()
    {
        // change color
        StartCoroutine(ColorChange());
    }

    private IEnumerator ColorChange()
    {
        
        float elapsedTime = 0;

        while (elapsedTime < changeTime)
        {
            m.color = Color.Lerp(off, on, (elapsedTime / changeTime));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        // finally, activate receiving elements
        Activate();
    }
}
