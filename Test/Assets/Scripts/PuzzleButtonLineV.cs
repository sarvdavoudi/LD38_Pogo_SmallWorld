﻿using System.Collections;
using System.Collections.Generic;


/*
 * Button makes a wide line shape when pushed
 * | O |
 * | O |
 * | O |
 * 
 */
public class PuzzleButtonLineV : PuzzleButton
{
    public PuzzleButtonLineV(int _x, int _y, PuzzleGrid _grid) : base(_x, _y, _grid)
    {
    }

    protected override void getAffected()
    {
        PuzzleBit bit;

        bit = grid.getBitAt(x, y - 1); // up
        if (bit != null) affectedBits.Add(bit);
        
        bit = grid.getBitAt(x, y); // center
        if (bit != null) affectedBits.Add(bit);

        bit = grid.getBitAt(x, y + 1); // down
        if (bit != null) affectedBits.Add(bit);

    }
}