﻿using System.Collections;
using System.Collections.Generic;


/*
 * Button makes a + shape when pushed
 * | O |
 * |OOO|
 * | O |
 * 
 */
public class PuzzleButtonPlus : PuzzleButton
{
    public PuzzleButtonPlus(int _x, int _y, PuzzleGrid _grid) : base(_x, _y, _grid)
    {
    }

    protected override void getAffected()
    {
        // + shape
        PuzzleBit bit;

        bit = grid.getBitAt(x, y - 1); // up
        if (bit != null) affectedBits.Add(bit);

        bit = grid.getBitAt(x - 1, y); // left
        if (bit != null) affectedBits.Add(bit);

        bit = grid.getBitAt(x, y); // center
        if (bit != null) affectedBits.Add(bit);

        bit = grid.getBitAt(x + 1, y); // right
        if (bit != null) affectedBits.Add(bit);
        
        bit = grid.getBitAt(x, y + 1); // down
        if (bit != null) affectedBits.Add(bit);
        
    }
}