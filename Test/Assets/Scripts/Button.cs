﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Button : MonoBehaviour {
    
    private PuzzleButton button;
    private bool btnEnabled;
    private Transform icon;
    private Color off;
    private Color on;
    private Color active;
    private List<Material> ms;
    private AudioSource aud;

    public void setButton(PuzzleButton btn, Transform _icon, Color _c)
    {
        button = btn;
        icon = _icon;
        icon.localScale = new Vector3(1, 1, 1);
        icon.localPosition = Vector3.zero;

        on = _c;
        off = Color.magenta; // should never see this
        active = Color.white;

        ms = new List<Material>();

        foreach (Renderer r in icon.GetComponentsInChildren<Renderer>())
        {
            ms.Add(r.material);
        }
        foreach (Material m in ms)
        {
            m.SetColor("_Color", off);
        }

        aud = GetComponent<AudioSource>();
    }

    public void Activate()
    {
        // distance check?
        button.toggle();
        button.grid.OnChange();
    }

    private void OnMouseDown()
    {
        if (btnEnabled)
        {
            Activate();
            aud.Play();
        }
    }

    public void enable()
    {
        btnEnabled = true;
        foreach (Material m in ms)
        {
            m.SetColor("_Color", on);
            m.SetColor("_EmissionColor", Color.black);
        }
    }

    public void disable()
    {
        btnEnabled = false;
        foreach (Material m in ms)
        {
            m.SetColor("_Color", off);
        }
    }

    public bool isEnabled()
    {
        return btnEnabled;
    }
    
    public void OnChange()
    {
        if (button.isActive)
        {
            foreach (Material m in ms)
            {
                m.SetColor("_Color", active);
                m.SetColor("_EmissionColor", Color.white);
            }
        }
        else
        {
            foreach (Material m in ms)
            {
                m.SetColor("_Color", on);
                m.SetColor("_EmissionColor", Color.black);
            }
        }
    }

    public void OnSolved()
    {
        btnEnabled = false;
    }
}
