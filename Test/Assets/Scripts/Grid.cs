﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : LogicActivatable {
    
    // colours
    public Color bitOff;
    public Color bitOn;
    public Color buttonIconColor;
    // object references
    public Transform screenCenter;
    public Bit bitPrefab;
    public Button buttonPrefab;
    public LogicActivatableFadeOut screen;

    // puzzle data
    public int sizeX;
    public int sizeY;
    public char[] puzzle;
    public bool[] solution;
    public bool[] activeButtonsOnStart;

    private PuzzleGrid grid; // model for data
    private Button[] buttons; // gameobjects
    private Bit[] bits; // gameobjects

    protected override void Init() {

        // create grid object
        grid = new PuzzleGrid(sizeX, sizeY, puzzle, solution, this);

        bits = new Bit[sizeX * sizeY];
        buttons = new Button[sizeX * sizeY];

        makeButtonsBits();
    }
    

    // change all arrays at once
    void OnValidate()
    {
        if (puzzle.Length != sizeX * sizeY)
        {
            //Debug.LogWarning("Don't change the 'puzzle' field's array size!");
            Array.Resize(ref puzzle, sizeX * sizeY);
        }
        
        if (solution.Length != sizeX * sizeY)
        {
            //Debug.LogWarning("Don't change the 'puzzle' field's array size!");
            Array.Resize(ref solution, sizeX * sizeY);
        }

        if (activeButtonsOnStart.Length != sizeX * sizeY)
        {
            //Debug.LogWarning("Don't change the 'puzzle' field's array size!");
            Array.Resize(ref activeButtonsOnStart, sizeX * sizeY);
        }
    }

    public void OnSolved()
    {
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                buttons[x + sizeX * y].OnSolved();
                bits[x + sizeX * y].OnSolved();
            }
        }
        Activate();
    }

    public void OnChange()
    {
        // update bits
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                bits[x + sizeX * y].OnChange();
                buttons[x + sizeX * y].OnChange();
            }
        }
    }

    protected override void DoActivate()
    {
        // enable all bits
        // enable all buttons
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                buttons[x + sizeX * y].enable();
                bits[x + sizeX * y].enable();
                if (activeButtonsOnStart[x + sizeX * y])
                {
                    buttons[x + sizeX * y].Activate();
                }
            }
        }

        screen.OnActivate();
    }

    private void makeButtonsBits()
    {
        Transform tBits = new GameObject("bits").transform;
        tBits.parent = screenCenter;
        tBits.localPosition = Vector3.zero;
        tBits.localRotation = Quaternion.identity;
        Transform tButtons = new GameObject("buttons").transform;
        tButtons.parent = screenCenter;
        tButtons.localPosition = Vector3.zero;
        tButtons.localRotation = Quaternion.identity;

        float gap = -0.25f;
        // some math to find the most left and bottom offset
        float offsetLeft = (-sizeX / 2f) * gap + gap / 2f;
        float offsetTop = (-sizeY / 2f) * gap + gap / 2f;
        // set it as first spawn position (z=1 because you had it in your script)
        Vector3 nextPosition = new Vector3(offsetLeft, offsetTop, 0f);

        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                // spawn button go, position, find and spawn icon, assign button go to model
                buttons[x + sizeX * y] = Instantiate(buttonPrefab, tButtons) as Button;
                buttons[x + sizeX * y].transform.localPosition = nextPosition;
                Transform t = Instantiate(PuzzleIconLUT.Instance.icons[puzzle[x + sizeX * y]], buttons[x + sizeX * y].transform);
                buttons[x + sizeX * y].setButton(grid.getButtonAt(x, y), t, buttonIconColor);

                // spawn bit, position, assign bit to model
                bits[x + sizeX * y] = Instantiate(bitPrefab, tBits) as Bit;
                bits[x + sizeX * y].transform.localPosition = nextPosition;
                bits[x + sizeX * y].setBit(grid.getBitAt(x, y), bitOff, bitOn);

                // add x distance
                nextPosition.y += gap;
            }
            // reset x position and add y distance
            nextPosition.y = offsetTop;
            nextPosition.x += gap;
        }

        // scale the grid so it fits in the screen
        float scaleFactor = 3f / Math.Max(sizeX, sizeY);
        Vector3 scale = new Vector3(scaleFactor, scaleFactor, 1f);
        tButtons.localScale = scale;
        tBits.localScale = scale;
    }
}