﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleBit {


    public int x;
    public int y;

    private bool lit;
    PuzzleButton litBy;

    public PuzzleBit(int _x, int _y)
    {
        x = _x;
        y = _y;
        lit = false;
        litBy = null;
    }
    
    public void activate(PuzzleButton button)
    {
        if (lit)
        { 
            litBy.toggle();
        }
        lit = true;
        litBy = button;
    }

    public void deactivate(PuzzleButton button)
    {
        litBy = null;
        lit = false;
    }

    public bool isLit()
    {
        return lit;
    }
}
