﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/*
 * Button makes a 3 wide line shape when pushed
 * |   |
 * |OOO|
 * |   |
 * 
 */
public class PuzzleButtonLineH : PuzzleButton
{
    public PuzzleButtonLineH(int _x, int _y, PuzzleGrid _grid) : base(_x, _y, _grid)
    {
    }

    protected override void getAffected()
    {
        // . shape
        PuzzleBit bit;
        
        bit = grid.getBitAt(x - 1, y); // left
        if (bit != null) affectedBits.Add(bit);

        bit = grid.getBitAt(x, y); // center
        if (bit != null) affectedBits.Add(bit);

        bit = grid.getBitAt(x + 1, y); // right
        if (bit != null) affectedBits.Add(bit);
    }
}