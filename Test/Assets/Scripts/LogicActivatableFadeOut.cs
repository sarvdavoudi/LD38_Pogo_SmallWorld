﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogicActivatableFadeOut : LogicActivatable {

    Material m;
    public float fadeTime = 1f;

    protected override void DoActivate()
    {
        StartCoroutine(FadeTo(0, fadeTime));
    }

    protected override void Init()
    {
        m = GetComponent<Renderer>().material;
    }
    
    IEnumerator FadeTo(float alphaTo, float changeTime)
    {
        float alphaFrom = m.color.a;
        float elapsedTime = 0;
        while (elapsedTime < changeTime)
        {
            m.SetColor("_Color", new Color(m.color.r, m.color.g, m.color.b, Mathf.Lerp(alphaFrom, alphaTo, (elapsedTime / changeTime))));
            elapsedTime += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }
        m.color = new Color(m.color.r, m.color.g, m.color.b, alphaTo);
    }
}
