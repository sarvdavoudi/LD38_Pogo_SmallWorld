﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleGrid {

    public int sizeX;
    public int sizeY;

    public bool solved = false;

    Grid grid;
    PuzzleBit[,] bits;
    PuzzleButton[,] buttons;
    bool[] solution;

    public PuzzleGrid(int _x, int _y, char[] puzzle, bool[] _solution, Grid _grid)
    {
        sizeX = _x;
        sizeY = _y;
        bits = new PuzzleBit[sizeX, sizeY];
        buttons = new PuzzleButton[sizeX, sizeY];
        
        // make bits
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                bits[x, y] = new PuzzleBit(x, y);
            }
        }
        // make buttons
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                // use class based on input
                char c = puzzle[x + sizeX * y];

                switch (c)
                {
                    case '.':
                        /* |   |
                         * | X |
                         * |   | */
                        buttons[x, y] = new PuzzleButtonDot(x, y, this);
                        break;
                    case '-':
                        /* |   |
                         * |XXX|
                         * |   | */
                        buttons[x, y] = new PuzzleButtonLineH(x, y, this);
                        break;
                    case 'i':
                        /* | X |
                         * | X |
                         * | X |  */
                        buttons[x, y] = new PuzzleButtonLineV(x, y, this);
                        break;
                    case '+':
                        /* | X |
                         * |XXX|
                         * | X | */
                        buttons[x, y] = new PuzzleButtonPlus(x, y, this);
                        break;
                    case 'D':
                        /* | X |
                         * | X |
                         * |XXX| */
                        buttons[x, y] = new PuzzleButtonTUp(x, y, this);
                        break;
                    case 'R':
                        /* |X  |
                         * |XXX|
                         * |X  | */
                        buttons[x, y] = new PuzzleButtonTRight(x, y, this);
                        break;
                    case 'L':
                        /* |  X|
                         * |XXX|
                         * |  X| */
                        buttons[x, y] = new PuzzleButtonTLeft(x, y, this);
                        break;
                    case 'T':
                        /* |XXX|
                         * | X |
                         * | X | */
                        buttons[x, y] = new PuzzleButtonTDown(x, y, this);
                        break;
                    case 'u':
                        /* |X X|
                         * |X X|
                         * |XXX| */
                        buttons[x, y] = new PuzzleButtonUUp(x, y, this);
                        break;
                    case '(':
                        /* |XXX|
                         * |X  |
                         * |XXX| */
                        buttons[x, y] = new PuzzleButtonURight(x, y, this);
                        break;
                    case ')':
                        /* |XXX|
                         * |  X|
                         * |XXX| */
                        buttons[x, y] = new PuzzleButtonULeft(x, y, this);
                        break;
                    case 'n':
                        /* |XXX|
                         * |X X|
                         * |X X| */
                        buttons[x, y] = new PuzzleButtonUDown(x, y, this);
                        break;
                    case 'v':
                        /* |X X|
                         * | X |
                         * |   | */
                        buttons[x, y] = new PuzzleButtonVUp(x, y, this);
                        break;
                    case '<':
                        /* |  X|
                         * | X |
                         * |  X| */
                        buttons[x, y] = new PuzzleButtonVRight(x, y, this);
                        break;
                    case '>':
                        /* |X  |
                         * | X |
                         * |X  | */
                        buttons[x, y] = new PuzzleButtonVLeft(x, y, this);
                        break;
                    case '^':
                        /* |   |
                         * | X |
                         * |X X| */
                        buttons[x, y] = new PuzzleButtonVDown(x, y, this);
                        break;
                    case 'x':
                        /* |   |
                         * |   |
                         * |   | */
                        buttons[x, y] = new PuzzleButtonFake(x, y, this);
                        break;
                    default:
                        buttons[x, y] = new PuzzleButtonFake(x, y, this);
                        break;
                }
            }
        }

        solution = _solution;
        grid = _grid;

    }
    
    public PuzzleBit getBitAt(int _x, int _y)
    {
        if (_x < sizeX && _y < sizeY && _x >= 0 && _y >= 0)
        {
            return bits[_x, _y];
        }
        else
        {
            Debug.Log("no such bit at: " + _x + "," + _y);
            return null;
        }
    }

    public PuzzleButton getButtonAt(int _x, int _y)
    {
        if (_x < sizeX && _y < sizeY && _x >= 0 && _y >= 0)
        {
            return buttons[_x, _y];
        }
        else
        {
            // no element exists
            Debug.Log("no such button at: " + _x + "," + _y);
            return null;
        }
    }

    public void OnChange()
    {
        grid.OnChange();
        if (isSolved())
        {
            // event? 
            solved = true;
            grid.OnSolved();
        } else
        {
            solved = false;
        }
    }

    private bool isSolved()
    {
        for (int x = 0; x < sizeX; x++)
        {
            for (int y = 0; y < sizeY; y++)
            {
                if (bits[x, y].isLit() != solution[x + sizeX * y])
                {
                    return false;
                }
            }
        }
        // if none are wrong
        return true;
    }
}