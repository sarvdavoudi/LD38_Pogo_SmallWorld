﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class LogicActivatable : MonoBehaviour
{
    // things that are activated by this
    public LogicActivatable[] activates;

    // delay before activating activatables
    public float delay;

    public bool startActive;

    public AudioClip activateSound;
    public AudioClip onActivateSound;
    private AudioSource aud;

    public int activatesRequired = 1;
    private int timesActivated = 0;

    // whether this has been previously activated
    public bool activated;

    // become activated
    public void OnActivate()
    {
        if (aud != null)
        {
            aud.PlayOneShot(onActivateSound);
        }
        DoActivate();
    }

    protected abstract void DoActivate();

    private void Start()
    {
        Init();
        if(activateSound != null || onActivateSound != null)
        {
            aud = gameObject.AddComponent<AudioSource>();
        }
        if (startActive)
        {
            Invoke("OnActivate", 1);//this will happen after 2 seconds
        }
    }

    protected abstract void Init();

    // activate all that follow this
    protected void Activate()
    {
        timesActivated++;
        if (!activated && timesActivated >= activatesRequired)
        {
            if (aud != null)
            {
                aud.PlayOneShot(activateSound);
            }
            activated = true;
            foreach (LogicActivatable ac in activates)
            {
                ac.Invoke("OnActivate", delay);
            }
        }
    }
}
