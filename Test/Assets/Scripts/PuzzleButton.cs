﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class PuzzleButton {

    public PuzzleGrid grid;
    public int x;
    public int y;

    public List<PuzzleBit> affectedBits;

    public bool isActive;
    
    public PuzzleButton(int _x, int _y,PuzzleGrid _grid)
    {
        init(_x, _y, _grid);
    }

    protected void init(int _x, int _y, PuzzleGrid _grid)
    {
        x = _x;
        y = _y;
        grid = _grid;
        
        affectedBits = new List<PuzzleBit>();
        getAffected();
    }

    protected abstract void getAffected();
    
    void activate()
    {
        foreach (PuzzleBit bit in affectedBits)
        {
            bit.activate(this);
        }
        isActive = true;
    }

    void deactivate()
    {
        foreach(PuzzleBit bit in affectedBits)
        {
            bit.deactivate(this);
        }
        isActive = false;
    }

    public void toggle()
    {
        if (isActive)
        {
            deactivate();
        }
        else
        {
            activate();
        }
    }
}
