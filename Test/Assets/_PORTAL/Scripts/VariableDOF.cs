﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;
using UnityEngine;

[ExecuteInEditMode]
public class VariableDOF : MonoBehaviour {

    Vector3 startScale;
    DepthOfFieldDeprecated script;
    Camera cam;

	// Use this for initialization
	void Start () {
        startScale = transform.localScale;
        cam = GetComponentInChildren<Camera>();
        script = cam.GetComponent<DepthOfFieldDeprecated>();
    }
	
	// Update is called once per frame
	void Update () {
        script.enabled = (transform.localScale.y < startScale.y * 0.9f) ? true : false;
    }
}
