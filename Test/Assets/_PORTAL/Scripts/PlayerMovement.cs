﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour {

	public float WalkSpeed     = 5.0f;
    public float RunSpeed      = 10.0f;
    public float Acceleration  = 4.90f;
    public float MouseXSpeed   = 75.0f;
    public float MouseYSpeed   = 75.0f;
    public float JumpForce     = 10.0f;
    public float Friction      = 0.15f;
    public float extraGravity  = 9.80f;
    public bool  Debug         = true;

    private Camera    _playerCam;
    private Rigidbody _rb;
    private Collider  _collider;
    private Vector3   _direction;
    private float     _yrotation = 0f;
    private float     _MaxSpeed;
    private float     _lastSpeedMax;

    private const float _viewRange = 90f;

    private bool _justJumped = false;

    /// Horizontal speed
    private float Speed
    {
        get { return Hvelocity.magnitude; }
        set { Hvelocity = Hvelocity.normalized * value; }
    }

    public Vector3 Direction
    {
        get { return _direction; }
    }

    /// Horizontal movement vector
    private Vector2 Hvelocity
    {
        get
        {
            // Truncate floating-point epsilon errors
            float _x = roundToNearest(_rb.velocity.x, 6);
            float _y = roundToNearest(_rb.velocity.z, 6);

            return new Vector2(_x, _y);
        }

        set
        {
            float _x = roundToNearest(value.x, 6);
            float _z = roundToNearest(value.y, 6);

            _rb.velocity = new Vector3(_x, _rb.velocity.y, _z);
        }
    }

    // Use this for initialization
    void Start () {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        _playerCam = GetComponentInChildren<Camera>();
        _collider = GetComponent<Collider>();
        _rb = GetComponent<Rigidbody>();

        //_rb.mass = 5.0f; // Make sure the mass hasn't been messed with

        _MaxSpeed = WalkSpeed;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		_direction = Vector3.zero;

        // Cardinal movement, get Raw axis to avoid smoothing
        _direction += transform.forward * Input.GetAxisRaw("Vertical");
        _direction += transform.right * Input.GetAxisRaw("Horizontal");
        _direction.Normalize();  // Prevent faster-on-diagonal movement

        // Mouse Horizontal
        transform.Rotate( Vector3.up * MouseXSpeed * Input.GetAxisRaw("Mouse X") * Time.deltaTime );

        // Mouse Vertical
        _yrotation += MouseYSpeed * Input.GetAxisRaw("Mouse Y") * Time.deltaTime;
        _yrotation = Mathf.Clamp(_yrotation, -_viewRange, _viewRange);  // Clamp vertical wraparound

        _playerCam.transform.localEulerAngles = new Vector3(-_yrotation, 0, 0);
    }

    void FixedUpdate(){

        // Sprint
        // Prevent switching speed limit while in air
        if (IsGrounded())
            _MaxSpeed = Input.GetKey(KeyCode.LeftShift) ? RunSpeed : WalkSpeed;

        // Friction
        // Do it before movement or else you never reach speed limit
        if (Speed > 0)
            Speed -= Speed * Friction;

        // Stop against a wall head-on
        //if (Input.GetAxisRaw("Vertical") > 0 && IsFrontColliding())
        //    Speed *= Friction;

        // Movement
        // Use rigidbody velocity to avoid collision jitter
        if(_direction.magnitude != 0)
            _rb.velocity += _direction * Acceleration;

        // Jump
        if (Input.GetKey(KeyCode.Space) && IsGrounded() && !_justJumped)
        {
            _rb.AddForce(Vector3.up * JumpForce, ForceMode.VelocityChange);
        }

        // Gravity
        _rb.AddForce(Vector3.down * extraGravity, ForceMode.Acceleration);

        //touchGround();

        // Velocity bounds
        if (Speed < 0.1f)
            Speed = 0f;

        if (_MaxSpeed - Speed <= 0.01f)
            Speed = _MaxSpeed;
    }

    void OnGUI(){
        if(Debug)
        {
            GUI.Label(
                new Rect(30, 10, 200, 200),
                string.Format(
                    "View Angles: {0}\n" +
                    "XYZ Velocity: {1}\n" +
                    "XY Velocity: {2}\n" +
                    "Speed: {3}\n" +
                    "Grounded: {4}\n" + 
                    "Direction: {5}\n",
                    _playerCam.transform.localEulerAngles.ToString(),
                    _rb.velocity.ToString(),
                    Hvelocity.ToString(),
                    Speed.ToString(),
                    IsGrounded().ToString(),
                    _direction.magnitude.ToString()
                )
            );
        }
	}

    private bool IsFrontColliding(){
        var _dw = _collider.bounds.extents.x + 0.1f;
        var _origin = _collider.bounds.center;
        var _dest = _origin + transform.forward * _dw;

        if(Debug)
            UnityEngine.Debug.DrawLine(_origin, _dest, Color.cyan);

        return Physics.Raycast(_collider.bounds.center, transform.forward, _dw);
    }

    private bool IsGrounded() {
        // Distance to ground
        float _dg = _collider.bounds.extents.y + 0.01f;
        var _origin = _collider.bounds.center;
        var _dest = _origin + Vector3.down * _dg;

        if(Debug)
            UnityEngine.Debug.DrawLine(_origin, _dest, Color.red);

        return Physics.Raycast(_origin, Vector3.down, _dg);
    }

    private float roundToNearest(float val, int digits)
    {
        float factor = Mathf.Pow(10, digits);

        return Mathf.Round(val * factor) / factor;
    }

    private bool nearTo(float a, float b, float epsilon)
    {
        return Mathf.Abs(a - b) < epsilon;
    }

    private Vector2 Normalized(Vector2 vec)
    {
        if(vec.magnitude == 0.0f)
            return vec;

        var factor = new Vector2(1 / vec.magnitude, 1 / vec.magnitude);
        vec.Scale(factor);
        return vec;
    }
}
