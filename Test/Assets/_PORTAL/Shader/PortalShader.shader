// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Shader created with Shader Forge v1.32 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.32;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:0,nrsp:0,vomd:0,spxs:True,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:33791,y:32510,varname:node_3138,prsc:2|emission-3343-OUT,olwid-7838-OUT,olcol-4217-OUT,voffset-8135-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:32167,y:32474,ptovrint:False,ptlb:RimColor,ptin:_RimColor,varname:node_7241,prsc:2,glob:False,taghide:True,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.7943541,c2:0.603,c3:1,c4:1;n:type:ShaderForge.SFN_Vector1,id:7838,x:33562,y:32917,varname:node_7838,prsc:2,v1:0.005;n:type:ShaderForge.SFN_Power,id:4217,x:32627,y:32577,varname:node_4217,prsc:2|VAL-7241-RGB,EXP-1282-OUT;n:type:ShaderForge.SFN_OneMinus,id:1282,x:32400,y:32330,varname:node_1282,prsc:2|IN-7241-RGB;n:type:ShaderForge.SFN_Tex2d,id:7319,x:32452,y:32049,ptovrint:False,ptlb:RenderTex,ptin:_RenderTex,varname:node_7319,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:a33dded51a1589547bc3bed74e0bb882,ntxv:1,isnm:False|UVIN-4609-UVOUT;n:type:ShaderForge.SFN_ScreenPos,id:4609,x:32141,y:32050,varname:node_4609,prsc:2,sctp:2;n:type:ShaderForge.SFN_Length,id:3203,x:32407,y:32837,varname:node_3203,prsc:2|IN-3775-OUT;n:type:ShaderForge.SFN_ObjectPosition,id:9079,x:31518,y:32966,varname:node_9079,prsc:2;n:type:ShaderForge.SFN_FragmentPosition,id:2580,x:31518,y:32795,varname:node_2580,prsc:2;n:type:ShaderForge.SFN_Subtract,id:2079,x:31962,y:32880,varname:node_2079,prsc:2|A-4229-XYZ,B-3487-XYZ;n:type:ShaderForge.SFN_ComponentMask,id:3775,x:32149,y:32880,varname:node_3775,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-2079-OUT;n:type:ShaderForge.SFN_Transform,id:4229,x:31761,y:32778,varname:node_4229,prsc:2,tffrom:0,tfto:1|IN-2580-XYZ;n:type:ShaderForge.SFN_Transform,id:3487,x:31761,y:32966,varname:node_3487,prsc:2,tffrom:0,tfto:1|IN-9079-XYZ;n:type:ShaderForge.SFN_Relay,id:664,x:33330,y:32857,varname:node_664,prsc:2|IN-690-OUT;n:type:ShaderForge.SFN_Relay,id:4785,x:33210,y:32508,varname:node_4785,prsc:2|IN-7319-RGB;n:type:ShaderForge.SFN_Lerp,id:3343,x:33458,y:32598,varname:node_3343,prsc:2|A-4785-OUT,B-7186-OUT,T-664-OUT;n:type:ShaderForge.SFN_Clamp01,id:690,x:33066,y:32935,varname:node_690,prsc:2|IN-3797-OUT;n:type:ShaderForge.SFN_Blend,id:7186,x:32934,y:32552,varname:node_7186,prsc:2,blmd:2,clmp:True|SRC-7319-RGB,DST-4217-OUT;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:3797,x:32780,y:32946,varname:node_3797,prsc:2|IN-3203-OUT,IMIN-5345-OUT,IMAX-4500-OUT,OMIN-8771-OUT,OMAX-4488-OUT;n:type:ShaderForge.SFN_Vector1,id:2610,x:32054,y:33235,varname:node_2610,prsc:2,v1:0.25;n:type:ShaderForge.SFN_Vector1,id:6528,x:32054,y:33310,varname:node_6528,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Vector1,id:3265,x:32013,y:33610,varname:node_3265,prsc:2,v1:-1;n:type:ShaderForge.SFN_Vector1,id:9931,x:32013,y:33663,varname:node_9931,prsc:2,v1:1;n:type:ShaderForge.SFN_Slider,id:7895,x:31934,y:33532,ptovrint:False,ptlb:oSlider,ptin:_oSlider,varname:node_7895,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Multiply,id:8771,x:32392,y:33449,varname:node_8771,prsc:2|A-3265-OUT,B-7895-OUT;n:type:ShaderForge.SFN_Multiply,id:4488,x:32392,y:33593,varname:node_4488,prsc:2|A-9931-OUT,B-7895-OUT;n:type:ShaderForge.SFN_Multiply,id:5345,x:32376,y:33089,varname:node_5345,prsc:2|A-2610-OUT,B-1719-OUT;n:type:ShaderForge.SFN_Multiply,id:4500,x:32376,y:33235,varname:node_4500,prsc:2|A-6528-OUT,B-1719-OUT;n:type:ShaderForge.SFN_Relay,id:1719,x:32208,y:33123,varname:node_1719,prsc:2|IN-7895-OUT;n:type:ShaderForge.SFN_Time,id:9445,x:31579,y:33172,varname:node_9445,prsc:2;n:type:ShaderForge.SFN_Time,id:5475,x:32941,y:33102,varname:node_5475,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:3074,x:33280,y:33246,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:8135,x:33598,y:33071,varname:node_8135,prsc:2|A-4956-OUT,B-1726-OUT;n:type:ShaderForge.SFN_Length,id:4956,x:33386,y:33032,varname:node_4956,prsc:2|IN-5705-OUT;n:type:ShaderForge.SFN_Multiply,id:1726,x:33504,y:33246,varname:node_1726,prsc:2|A-3074-OUT,B-2223-OUT;n:type:ShaderForge.SFN_Vector1,id:2223,x:33280,y:33415,varname:node_2223,prsc:2,v1:0.005;n:type:ShaderForge.SFN_Sin,id:5705,x:33170,y:33074,varname:node_5705,prsc:2|IN-5475-T;proporder:7241-7319-7895;pass:END;sub:END;*/

Shader "Shader Forge/PortalShader" {
    Properties {
        [HideInInspector]_RimColor ("RimColor", Color) = (0.7943541,0.603,1,1)
        _RenderTex ("RenderTex", 2D) = "gray" {}
        _oSlider ("oSlider", Range(0, 1)) = 1
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "Outline"
            Tags {
            }
            Cull Front
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _RimColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_5475 = _Time + _TimeEditor;
                v.vertex.xyz += (length(sin(node_5475.g))*(v.normal*0.005));
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(float4(v.vertex.xyz + v.normal*0.005,1) );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 node_4217 = pow(_RimColor.rgb,(1.0 - _RimColor.rgb));
                return fixed4(node_4217,0);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float4 _RimColor;
            uniform sampler2D _RenderTex; uniform float4 _RenderTex_ST;
            uniform float _oSlider;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                float4 screenPos : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 objPos = mul ( unity_ObjectToWorld, float4(0,0,0,1) );
                float4 node_5475 = _Time + _TimeEditor;
                v.vertex.xyz += (length(sin(node_5475.g))*(v.normal*0.005));
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float4 objPos = mul ( unity_ObjectToWorld, float4(0,0,0,1) );
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5;
////// Lighting:
////// Emissive:
                float4 _RenderTex_var = tex2D(_RenderTex,TRANSFORM_TEX(sceneUVs.rg, _RenderTex));
                float3 node_4217 = pow(_RimColor.rgb,(1.0 - _RimColor.rgb));
                float node_1719 = _oSlider;
                float node_5345 = (0.25*node_1719);
                float node_8771 = ((-1.0)*_oSlider);
                float3 emissive = lerp(_RenderTex_var.rgb,saturate((1.0-((1.0-node_4217)/_RenderTex_var.rgb))),saturate((node_8771 + ( (length((mul( unity_WorldToObject, float4(i.posWorld.rgb,0) ).xyz.rgb-mul( unity_WorldToObject, float4(objPos.rgb,0) ).xyz.rgb).rg) - node_5345) * ((1.0*_oSlider) - node_8771) ) / ((0.5*node_1719) - node_5345))));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_5475 = _Time + _TimeEditor;
                v.vertex.xyz += (length(sin(node_5475.g))*(v.normal*0.005));
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
